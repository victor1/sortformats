import sys
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga","pdf", "svg", "png", "gif", "jpeg", "webp")
#"raw"< "bmp"< "tiff"< "ppm"< "eps"< "tga"< "pdf"< "svg"< "png"< "gif"< "jpeg"< "webp"

def lower_than(format1: str, format2: str):
    pos1=0
    pos2=0
    for x in range(0,len(fordered)):
        if format1 == fordered[x]:
            pos1 = x
        elif format2 == fordered[x]:
            pos2 = x
    if pos1 < pos2:
        return True
    elif pos1 >= pos2:
        return False

def find_lower_pos(formats: list, pivot: int):
    lower: int = pivot
    for pos in range(pivot, len(formats)):

        if lower_than(formats[pos],formats[pivot]):
            lower = pos
    return lower

def sort_formats(formats: list) -> list:
        if len(formats) <= 1:
            return formats

        pivot = formats[0]
        left = []
        right = []

        for i in range(1, len(formats)):
            if lower_than(formats[i], pivot):
                left.append(formats[i])
            else:
                right.append(formats[i])

        left = sort_formats(left)
        right = sort_formats(right)

        return left + [pivot] + right

def main ():
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")

if __name__ == '__main__':
    main()
